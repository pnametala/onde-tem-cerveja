package com.pnametala.debarembar.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pnametala.debarembar.R;
import com.pnametala.debarembar.adapters.ItemsInPlaceAdapter;
import com.pnametala.debarembar.entities.PlaceItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppetizersFragment extends Fragment {

    private ItemsInPlaceAdapter adapter;
    private ListView lv;
    public AppetizersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.appetizers_in_place, container,false);
        lv = (ListView) view.findViewById(R.id.appetizers_in_place);
        lv.setEmptyView(view.findViewById(R.id.empty));
        ArrayList<PlaceItem> items = new ArrayList<>();
        adapter = new ItemsInPlaceAdapter(getActivity(), items);
        lv.setAdapter(adapter);

        return view;
    }

    public void refresh(List<PlaceItem> items) {
        this.adapter.refreshListView(items);
    }
}
