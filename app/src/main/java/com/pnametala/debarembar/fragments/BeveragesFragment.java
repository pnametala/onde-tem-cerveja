package com.pnametala.debarembar.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pnametala.debarembar.R;
import com.pnametala.debarembar.adapters.ItemsInPlaceAdapter;
import com.pnametala.debarembar.entities.PlaceItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeveragesFragment extends Fragment {

    private ItemsInPlaceAdapter adapter;
    private ListView lv;
    public BeveragesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.beverages_in_place, container,false);
        lv = (ListView) view.findViewById(R.id.beverages_in_place);
        lv.setEmptyView(view.findViewById(R.id.empty));
        ArrayList<PlaceItem> items = new ArrayList<>();
        lv.setAdapter(new ItemsInPlaceAdapter(getActivity(), items));
        this.adapter = new ItemsInPlaceAdapter(getActivity(), items);
        lv.setAdapter(this.adapter);

        return view;
    }

    public void refresh(List<PlaceItem> items) {
        this.adapter.refreshListView(items);
    }

    /*@Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        PlaceItem pi =  (PlaceItem) adapterView.getItemAtPosition(i);
        final String[] selectedItemType = new String[1];
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.add_item, (ViewGroup) getActivity().findViewById(R.id.layout_root));
        //layout_root should be the name of the "top-level" layout node in the dialog_layout.xml file.
        final EditText itemName = (EditText) layout.findViewById(R.id.item_name);
        final EditText itemValue = (EditText) layout.findViewById(R.id.item_value);
        final MaterialBetterSpinner dropdown = (MaterialBetterSpinner) layout.findViewById(R.id.item_type);
        ArrayAdapter<String> dropdownAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, getActivity().getResources().getStringArray(R.array.itemTypes));
        dropdown.setAdapter(dropdownAdapter);

        itemName.setText(pi.getItemName());
        itemValue.setText(String.valueOf(pi.getItemPrice()));
        dropdown.setText(pi.getItemType());
        selectedItemType[0] = pi.getItemType();

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(layout);

        dropdown.setOnItemClickListener((adapterView1, view1, i1, ll) -> {
            selectedItemType[0] = (String) adapterView1.getItemAtPosition(i1);
        });
        builder.setPositiveButton("Adicionar", (dialog, j) ->  {
            if(selectedItemType[0] == null || itemName.getText().toString().isEmpty() || itemValue.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), "Por favor, complete os campos", Toast.LENGTH_SHORT).show();
                return;
            }
            dialog.dismiss();
            //save info where you want it
            pi.setItemName(itemName.getText().toString());
            pi.setItemPrice(Double.valueOf(itemValue.getText().toString()));
            pi.setItemType(selectedItemType[0]);
            pi.saveInBackground(e -> {
                if(e == null) {
                    this.adapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Item atualizado", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Erro ao salvar", Toast.LENGTH_SHORT).show();
                    Log.e("PARSE", e.getMessage());
                }
            });
        });
        builder.setNegativeButton("Cancelar", (dialog, k) ->  {
            dialog.dismiss();
        });
        AlertDialog dialog = builder.create();
        dialog.show();

        return true;
    }
    */
}
