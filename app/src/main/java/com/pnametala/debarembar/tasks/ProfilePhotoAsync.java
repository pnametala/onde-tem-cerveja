package com.pnametala.debarembar.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.pnametala.debarembar.interfaces.AsyncResponse;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by visagio on 24/08/16.
 */
public class ProfilePhotoAsync extends AsyncTask<String, String, Bitmap> {
    public Bitmap bitmap;
    String url;
    private Context context;
    public AsyncResponse<Bitmap> delegate;

    public ProfilePhotoAsync(String url) {
        this.url = url;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        // Fetching data from URI and storing in bitmap
        bitmap = DownloadImageBitmap(url);
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap s) {
        //super.onPostExecute(s);
        delegate.processFinish(s);
    }

    public static Bitmap DownloadImageBitmap(String url) {
        Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e("IMAGE", "Error getting bitmap", e);
        }
        return bm;
    }
}
