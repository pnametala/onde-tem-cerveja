package com.pnametala.debarembar.listeners;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseUser;
import com.pnametala.debarembar.ManageAccountActivity;
import com.pnametala.debarembar.R;
import com.pnametala.debarembar.SplashActivity;

/**
 * Created by visagio on 13/08/16.
 */
public class DrawerItemClickListener implements NavigationView.OnNavigationItemSelectedListener {
    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private View layout;
    private AutoCompleteTextView mapSearcher;
    private TextView placeName;
    private LatLng latLng;
    private String placeAddress;
    private ViewGroup vg;
    private Intent intent;

    public DrawerItemClickListener  (Context context, GoogleApiClient googleApiClient, ViewGroup viewGroup) {
        mContext = context;
        mGoogleApiClient = googleApiClient;
        vg = viewGroup;
    }
    private boolean rateApp(Context appContext, Intent appIntent) {
        try {
            appContext.startActivity(appIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //if(menuItem.isChecked()) menuItem.setChecked(false);
        //else menuItem.setChecked(true);
        switch (item.getItemId()) {
            /*case R.id.manage_account:
                intent = new Intent(mContext, ManageAccountActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                return true;
                */
            case R.id.rate_app: //rate app
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri
                        .parse("market://details?id=com.pnametala.debarembar"));
                if (!rateApp(mContext, intent)) {
                    // Market (Google play) app seems not installed, let's try to
                    // open a webbrowser
                    intent.setData(Uri
                            .parse("https://play.google.com/store/apps/details?id=com.pnametala.debarembar"));
                    if (!rateApp(mContext, intent)) {
                        // Well if this also fails, we have run out of options,
                        // inform the user.
                        Toast.makeText(mContext, "Não foi possível redirecionar para a Play Store.", Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            case R.id.log_out:
                ParseUser.getCurrentUser().logOut();
                intent = new Intent(mContext, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                return true;
            default:
                return true;
        }
    }
}
