package com.pnametala.debarembar.listeners;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.pnametala.debarembar.UserLocation;

/**
 * Created by visagio on 25/07/16.
 */
public class UserLocationListener implements LocationListener {

    @Override
    public void onLocationChanged(Location location) {
        UserLocation.LOCATION = location;
        UserLocation.LAT = location.getLatitude();
        UserLocation.LONG = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
