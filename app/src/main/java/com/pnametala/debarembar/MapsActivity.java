package com.pnametala.debarembar;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pnametala.debarembar.adapters.AutoCompleteAdapter;
import com.pnametala.debarembar.adapters.ItemsInPlaceAdapter;
import com.pnametala.debarembar.adapters.TabsAdapter;
import com.pnametala.debarembar.entities.PlaceItem;
import com.pnametala.debarembar.entities.PlacePrediction;
import com.pnametala.debarembar.entities.UserPlace;
import com.pnametala.debarembar.fragments.BeveragesFragment;
import com.pnametala.debarembar.interfaces.OnInitActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.security.Permission;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback,
        OnInitActivity,
        GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemClickListener, GoogleMap.OnMarkerClickListener {

    private static final int MY_PERMISSIONS_REQUEST = 0;

    private GoogleMap mMap;

    private View bottomSheet;
    private BottomSheetBehavior mBottomSheetBehavior;
    private AutoCompleteTextView mapSearcher;
    private ListView beveragesInPlace;
    private ListView appetizersInPlace;
    private TextView placeName;
    private TextView placeAddress;
    private TextView placePhone;
    private RatingBar placeRating;
    private ImageView placePicture;

    /*Menu*/
    private FloatingActionButton myLocation;
    private FloatingActionButton placeAdd;

    private UserLocation userLocation;
    private ArrayList<PlaceItem> beveragesPlaceItems;
    private ArrayList<PlaceItem> appetizersPlaceItems;
    private ItemsInPlaceAdapter beveragesAdapter;
    private ItemsInPlaceAdapter appetizersAdapter;

    /*Tabs*/
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabsAdapter tabsAdapter;
    private BeveragesFragment beveragesFragment;
    private Button addItem;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private AutoCompleteTextView addPlaceSearcher;
    private TextView addPlaceName;
    private LatLng addLatLng;
    private String addPlaceAddress;
    private UserPlace chosenPlace;

    //item add
    private EditText itemName;
    private EditText itemValue;
    private MaterialBetterSpinner dropdown;
    private String selectedItemType;
    private MaterialBetterSpinner dropdownComplement;
    private String selectedComplement;
    private LocationManager locationManager;

    private final Integer cameraZoom = 14;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private final String[] items = getResources().getStringArray(R.array.itemTypes);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        init();
        initAppBar();
        leftMenu();

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            mapIsReadyAndHasPermissions();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Permissao negada", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                mapIsReadyAndHasPermissions();
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void mapIsReadyAndHasPermissions() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser();
        } else {
            userLocation = new UserLocation(getBaseContext());
            LatLng ll = new LatLng(-22.9103553, -43.7285219);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
            if(UserLocation.LOCATION != null) {
                ll = new LatLng(UserLocation.LOCATION.getLatitude(), UserLocation.LOCATION.getLongitude());
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, cameraZoom));

            loadNearbyPlaces(ll, 10); //kilometers
            initCameraListener();
        }
    }

    private void initCameraListener() {
        mMap.setOnCameraIdleListener(() -> {
            loadNearbyPlaces(mMap.getCameraPosition().target, 10);
        });
    }

    private void loadNearbyPlaces(LatLng ll, double radius) {
        /*Bitmap images*/
        LatLng southwest = SphericalUtil.computeOffset(ll, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(ll, radius * Math.sqrt(2.0), 45);
        LatLngBounds llBounds = new LatLngBounds(southwest, northeast);

        BitmapDescriptor bar = BitmapDescriptorFactory.fromResource(R.drawable.cerveja_amarelo_fundo);
        ParseQuery<UserPlace> query = ParseQuery.getQuery(UserPlace.class);
        query.whereWithinKilometers("location", new ParseGeoPoint(ll.latitude, ll.longitude), radius);
        /*query.whereWithinGeoBox("location",
                new ParseGeoPoint(southwest.latitude, southwest.longitude)
                , new ParseGeoPoint(northeast.latitude, northeast.longitude));*/
        query.findInBackground((placeList, e) -> {
            if (e == null) {
                for (UserPlace p :
                        placeList) {
                    LatLng place = new LatLng(p.getLocation().getLatitude(), p.getLocation().getLongitude());
                    mMap.addMarker(new MarkerOptions()
                            .position(place)
                            .title(p.getName())
                            .snippet(p.getObjectId())
                            .icon(bar));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(home));
                    mMap.setOnMarkerClickListener(this);

                }
            } else {

                Log.d("score", "Error: " + e.getMessage());
            }
        });
    }

    @Override
    public void init() {
        googleApiInit();
        autoCompleteInit();
        tabsInit();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        this.bottomSheet = (View) findViewById(R.id.bottom_sheet);
        this.placeName = (TextView) findViewById(R.id.place_name);
        this.myLocation = (FloatingActionButton) findViewById(R.id.my_location);
        this.placeAdd = (FloatingActionButton) findViewById(R.id.place_add);
        this.placeAddress = (TextView) findViewById(R.id.place_address);
        this.placePhone = (TextView) findViewById(R.id.place_phone);
        //this.placeRating = (RatingBar) findViewById(R.id.place_rating);
        this.beveragesPlaceItems = new ArrayList<PlaceItem>();
        this.appetizersPlaceItems = new ArrayList<PlaceItem>();
        this.beveragesAdapter = new ItemsInPlaceAdapter(getBaseContext(), beveragesPlaceItems);
        this.appetizersAdapter = new ItemsInPlaceAdapter(getBaseContext(), appetizersPlaceItems);
        this.addItem = (Button) findViewById(R.id.add_item);

        findViewById(R.id.coordinator_layout).post(new Runnable() {
            @Override
            public void run() {
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) bottomSheet.getLayoutParams();
                params.height = findViewById(R.id.coordinator_layout).getHeight() - findViewById(R.id.toolbar).getHeight();
                bottomSheet.setLayoutParams(params);
            }
        });

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setPeekHeight(0);
                    myLocation.setVisibility(View.VISIBLE);
                    placeAdd.setVisibility(View.VISIBLE);
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    myLocation.setVisibility(View.GONE);
                    placeAdd.setVisibility(View.GONE);
                } else if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });

        this.myLocation.setOnClickListener(view1 -> {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showGPSDisabledAlertToUser();
            } else {
                userLocation.getLocation();
                LatLng ll = new LatLng(UserLocation.LOCATION.getLatitude(), UserLocation.LOCATION.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, cameraZoom));
                loadNearbyPlaces(ll, 10);
            }
        });

        this.placeAdd.setOnClickListener(view -> {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.create_place, (ViewGroup) findViewById(R.id.layout_root));
            //layout_root should be the name of the "top-level" layout node in the dialog_layout.xml file.
            addPlaceSearcher = (AutoCompleteTextView) layout.findViewById(R.id.map_searcher);
            addPlaceName = (TextView) layout.findViewById(R.id.place_name);
            addLatLng = null;
            addPlaceAddress = null;

            AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(this);
            autoCompleteAdapter.setGoogleApiClient(mGoogleApiClient);
            addPlaceSearcher.setAdapter(autoCompleteAdapter);
            addPlaceSearcher.setThreshold(1);
            addPlaceSearcher.setOnItemClickListener((adapterView, view1, i1, l) -> {
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getWindowToken(), 0);
                PlacePrediction pp = (PlacePrediction) adapterView.getItemAtPosition(i1);
                addPlaceSearcher.setText(pp.getDescription());
                PendingResult<PlaceBuffer> result = Places.GeoDataApi.getPlaceById(mGoogleApiClient, pp.getPlaceId());
                result.setResultCallback(places -> {
                    if(places == null) return;

                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                        Place place = places.get(0);
                        addPlaceSearcher.setText(place.getAddress());
                        addLatLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                        addPlaceAddress  =  place.getAddress().toString();
                    }
                    //Prevent memory leak by releasing buffer
                    places.release();
                });
            });
            //Building dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder.setView(layout);

            builder.setPositiveButton("Adicionar", (dialog, i) ->  {
                //save info where you want it
                if(addPlaceName.getText().toString().isEmpty() || addPlaceAddress.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Por favor, complete os campos", Toast.LENGTH_SHORT).show();
                    return;
                }
                UserPlace up = new UserPlace();
                up.setName(addPlaceName.getText().toString());
                up.setLocation(new ParseGeoPoint(addLatLng.latitude,  addLatLng.longitude));
                up.setAddress(addPlaceAddress);
                up.saveInBackground(e -> {
                    if(e == null) {
                        //move camera to new bar
                        Toast.makeText(getBaseContext(), "Bar salvo com sucesso", Toast.LENGTH_SHORT).show();
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(addLatLng));
                        mMap.clear();
                        loadNearbyPlaces(addLatLng, 10);
                    } else {
                        Toast.makeText(getBaseContext(), "Erro ao salvar", Toast.LENGTH_SHORT).show();
                        Log.e("PARSE", e.getMessage());
                    }
                });
                dialog.dismiss();
            });
            builder.setNegativeButton("Cancelar", (dialog, i) ->  {
                dialog.dismiss();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        //item add
        this.addItem.setOnClickListener(view -> {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.add_item, (ViewGroup) findViewById(R.id.layout_root));
            //layout_root should be the name of the "top-level" layout node in the dialog_layout.xml file.
            itemName = (EditText) layout.findViewById(R.id.item_name);
            itemValue = (EditText) layout.findViewById(R.id.item_value);
            dropdown = (MaterialBetterSpinner) layout.findViewById(R.id.item_type);
            dropdownComplement = (MaterialBetterSpinner) layout.findViewById(R.id.item_complement);
            ArrayList<String> arrBeveragesComplement = new ArrayList<>();
            ArrayList<String> arrAppetizersComplement = new ArrayList<>();
            Collections.addAll(arrBeveragesComplement, getResources().getStringArray(R.array.beverageTypes));
            Collections.addAll(arrAppetizersComplement, getResources().getStringArray(R.array.appetizerTypes));
            ArrayAdapter<String> dropdownAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.itemTypes));
            ArrayAdapter<String> dropdownComplementAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>());
            dropdown.setAdapter(dropdownAdapter);
            dropdownComplement.setAdapter(dropdownComplementAdapter);
            selectedItemType = null;
            selectedComplement = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
            builder.setView(layout);

            dropdown.setOnItemClickListener((adapterView, view1, i1, l) -> {
                selectedItemType = (String) adapterView.getItemAtPosition(i1);
                switch (i1) {
                    case 0: //Beverages
                        dropdownComplementAdapter.clear();
                        dropdownComplementAdapter.addAll(arrBeveragesComplement);
                        break;
                    case 1: //Appetizers
                        dropdownComplementAdapter.clear();
                        dropdownComplementAdapter.addAll(arrAppetizersComplement);
                        break;
                    default:
                        break;
                }

                dropdownComplementAdapter.notifyDataSetChanged();
            });
            dropdownComplement.setOnItemClickListener((adapterView1, view2, ik, ll) -> {
                selectedComplement = (String) adapterView1.getItemAtPosition(ik);
            });
            builder.setPositiveButton("Adicionar", (dialog, i) ->  {
                boolean validPrice = true;

                try {
                    Double.parseDouble(itemValue.getText().toString());
                }  catch (NumberFormatException e) {
                    validPrice = false;
                }
                if(selectedItemType  == null || selectedComplement == null || itemName.getText().toString().isEmpty() || !validPrice) {
                    Toast.makeText(getBaseContext(), "Por favor, complete os campos", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();
                //save info where you want it
                PlaceItem item = new PlaceItem();
                item.setItemName(itemName.getText().toString());
                item.setItemPrice(Double.valueOf(itemValue.getText().toString()));
                item.setItemType(selectedItemType);
                item.setItemComplement(selectedComplement);
                item.createdBy(ParseUser.getCurrentUser());
                item.setRatingCount(0);
                item.saveInBackground(e -> {
                    if(e == null) {
                        chosenPlace.setPlaceItem(item);
                        chosenPlace.saveInBackground(e1 -> {
                            if(e==null)  {
                                tabsAdapter.refresh(chosenPlace);
                                Toast.makeText(getBaseContext(), "Bar atualizado", Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else {
                        Toast.makeText(getBaseContext(), "Erro ao salvar", Toast.LENGTH_SHORT).show();
                        Log.e("PARSE", e.getMessage());
                    }
                });
            });
            builder.setNegativeButton("Cancelar", (dialog, i) ->  {
                dialog.dismiss();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    private void tabsInit() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabsAdapter = new TabsAdapter(getSupportFragmentManager(), getBaseContext());
        viewPager.setAdapter(tabsAdapter);
        viewPager.setOffscreenPageLimit(3);
        final TabLayout.Tab beverages = tabLayout.newTab();
        final TabLayout.Tab appetizers = tabLayout.newTab();
        beverages.setText("Bebidas");
        appetizers.setText("Comidas");
        tabLayout.addTab(beverages, 0);
        tabLayout.addTab(appetizers, 1);

        //this.beveragesInPlace = (ListView) findViewById(R.id.beverages_in_place);
        //this.appetizersInPlace = (ListView) findViewById(R.id.appetizers_in_place);
        this.beveragesInPlace = new ListView(getBaseContext());

        this.appetizersInPlace = new ListView(getBaseContext());

        this.beveragesInPlace.setAdapter(beveragesAdapter);
        this.appetizersInPlace.setAdapter(appetizersAdapter);
        /*Vector<View> pages = new Vector<View>();

        pages.add(beveragesInPlace);
        pages.add(appetizersInPlace);*/
        //tabsAdapter = new TabsAdapter(getSupportFragmentManager(), getBaseContext(), pages);

        beveragesFragment = (BeveragesFragment) ((TabsAdapter) viewPager.getAdapter()).getItem(0);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void autoCompleteInit() {
        this.mapSearcher = (AutoCompleteTextView) findViewById(R.id.map_searcher);

        AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(this);
        autoCompleteAdapter.setGoogleApiClient(mGoogleApiClient);
        this.mapSearcher.setAdapter(autoCompleteAdapter);

        this.mapSearcher.setThreshold(1);
        this.mapSearcher.setOnItemClickListener(this);
    }

    public void googleApiInit() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        if (mGoogleApiClient != null) mGoogleApiClient.connect();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.pnametala.debarembar/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.pnametala.debarembar/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Log.d("connection_error", connectionResult.getErrorMessage());
        connectionResult.getResolution();
        Toast.makeText(this, "Não foi possível conectar no Google API", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), 0);
        PlacePrediction pp = (PlacePrediction) adapterView.getItemAtPosition(i);
        mapSearcher.setText(pp.getDescription());
        PendingResult<PlaceBuffer> result = Places.GeoDataApi.getPlaceById(mGoogleApiClient, pp.getPlaceId());
        result.setResultCallback(
                new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer buffer) {
                        if (buffer == null)
                            return;
                        if (buffer.getStatus().isSuccess() && buffer.getCount() > 0) {
                            Place place = buffer.get(0);
                            //mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName().toString()));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                            mMap.clear();
                            loadNearbyPlaces(place.getLatLng(), 10);
                        }
                        //Prevent memory leak by releasing buffer
                        buffer.release();
                    }
                }, 60, TimeUnit.SECONDS);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        ParseQuery<UserPlace> query = ParseQuery.getQuery(UserPlace.class);
        query.whereEqualTo("objectId", marker.getSnippet());
        query.getFirstInBackground((place, e) -> {
            if (e == null) {
                chosenPlace = place;
                //beveragesPlaceItems.clear();
                //appetizersPlaceItems.clear();
                tabsAdapter.refresh(place);
                //beveragesPlaceItems.addAll(beverages);
                //appetizersPlaceItems.addAll(appetizers);
                placeName.setText(place.getName());
                placeName.setKeyListener(null);
                placeAddress.setText(place.getAddress());
                placeAddress.setKeyListener(null);
                if(place.getPhone() != null) {
                    placePhone.setText(PhoneNumberUtils.formatNumber(place.getPhone()));
                } else{
                    placePhone.setVisibility(View.GONE);
                }
                placePhone.setKeyListener(null);
            }
        });
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(mBottomSheetBehavior.getState()  !=  BottomSheetBehavior.STATE_COLLAPSED)  {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }
}
