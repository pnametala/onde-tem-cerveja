package com.pnametala.debarembar.interfaces;

/**
 * Created by visagio on 24/08/16.
 */
public interface AsyncResponse<T> {
    void processFinish(T output);
}
