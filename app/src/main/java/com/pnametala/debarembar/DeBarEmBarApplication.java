package com.pnametala.debarembar;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.Keep;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.pnametala.debarembar.entities.PlaceItem;
import com.pnametala.debarembar.entities.PlaceItemRating;
import com.pnametala.debarembar.entities.UserPlace;

/**
 * Created by visagio on 09/08/16.
 */

public class DeBarEmBarApplication extends Application {

    private Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();

        //Your code
        FacebookSdk.sdkInitialize(getBaseContext());
        //AppEventsLogger.activateApp(this);
        //registerParseSubClasses();
        ParseObject.registerSubclass(UserPlace.class);
        ParseObject.registerSubclass(PlaceItem.class);
        ParseObject.registerSubclass(PlaceItemRating.class);
        //parseInit();
        Parse.initialize(new Parse.Configuration.Builder(getBaseContext())
                //.applicationId("DEBAREMBARAPPID")
                .applicationId("XZzUa4PxPQczNX81zKghCybEnPDcRWBs")
                //.server("http://debarembar.herokuapp.com/parse/")
                .server("http://67.205.134.126:1337/parse")
                .enableLocalDataStore()
                .build());
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        ParseFacebookUtils.initialize(getBaseContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        mContext = base;
    }

    /*
    private void registerParseSubClasses() {
        //registering subclasses
        ParseObject.registerSubclass(UserPlace.class);
        ParseObject.registerSubclass(PlaceItem.class);
        ParseObject.registerSubclass(PlaceItemRating.class);
    }
    private void parseInit() {
        Parse.initialize(new Parse.Configuration.Builder(getBaseContext())
                .applicationId("DEBAREMBARAPPID")
                .server("http://debarembar.herokuapp.com/parse/")
                //.enableLocalDataStore()
                .build());
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        ParseFacebookUtils.initialize(this);
    }
    */
}
