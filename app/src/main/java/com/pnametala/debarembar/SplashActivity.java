package com.pnametala.debarembar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pnametala.debarembar.interfaces.AsyncResponse;
import com.pnametala.debarembar.tasks.ProfilePhotoAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by visagio on 14/08/16.
 */
public class SplashActivity extends Activity implements AsyncResponse<Bitmap> {
    private final int SPLASH_DISPLAY_LENGHT = 3000;
    private Button facebookLogin;
    private AccessToken token;
    Profile mFbProfile;
    ParseUser parseUser;
    private List<String> permissions;

    private String name;
    private String email;

    private EditText loginEmail;
    private EditText loginPassword;
    private Button loginEnter;
    private ProgressDialog progressDialog;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private Bitmap profilePic;
    private String id;
    private ProfilePhotoAsync photoAsync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        fbKeyLog();
        if(!FacebookSdk.isInitialized()) Log.e("De Bam Em Bar", "Facebook SDK is not initialized");
        token = AccessToken.getCurrentAccessToken();

        progressDialog = ProgressDialog.show(this, "Login", "Verificando usuário", true);

        if (token != null && !token.isExpired()) {
            ParseFacebookUtils.logInInBackground(token, (user, e) -> {
                if (user.isNew()) {
                    Log.d("De Bar Em Bar", "User signed up and logged in through Facebook!");
                    getUserDetailsFromFB();
                } else {
                    Log.d("De Bar Em Bar", "User logged in through Facebook!");
                    getUserDetailsFromParse();
                }
            });
        } else {
            progressDialog.dismiss();
        }

        permissions = new ArrayList<String>() {{
            add("public_profile");
            add("email");
        }};

        //loginEmail = (EditText) findViewById(R.id.user_login);
        //loginPassword = (EditText) findViewById(R.id.user_password);
        //loginEnter = (Button) findViewById(R.id.user_sign_in);
        facebookLogin = (Button) findViewById(R.id.facebook_login);
        facebookLogin.setOnClickListener(view -> {
            ParseFacebookUtils.logInWithReadPermissionsInBackground(SplashActivity.this, permissions, (user, e) -> {
                if (e == null) {
                    if (user == null) {
                        Log.d("De Bar Em Bar", "Uh oh. The user cancelled the Facebook login.");
                    } else if (user.isNew()) {
                        Log.d("De Bar Em Bar", "User signed up and logged in through Facebook!");
                        getUserDetailsFromFB();
                    } else {
                        Log.d("De Bar Em Bar", "User logged in through Facebook!");
                        getUserDetailsFromParse();
                    }
                } else {
                    Log.e("De Bar Em Bar", e.getMessage());
                }
            });
        });

        /*loginEnter.setOnClickListener(view -> {
            if(loginEmail.getText().toString().isEmpty() || loginPassword.getText().toString().isEmpty()) {
                Toast.makeText(getBaseContext(), "Por favor, preencha os campos de e-mail e senha", Toast.LENGTH_SHORT).show();
                return;
            }
            String login = loginEmail.getText().toString();
            String password = loginPassword.getText().toString();
            ParseUser.logInInBackground(login, password, (user, e) -> {
                if(e == null) {
                    if (user != null) {
                        //logged user
                        if(user.getString("name").isEmpty()) {
                            //call complete activity
                        }
                        loadMapActivity();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Ocorreu um erro ao tentar logar. Tente novamente mais tarde...", Toast.LENGTH_SHORT).show();
                }
            });

        });*/
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }
    private void loadMapActivity() {
        Intent mainIntent = new Intent(SplashActivity.this, MapsActivity.class);
        SplashActivity.this.startActivity(mainIntent);
        progressDialog.dismiss();
        SplashActivity.this.finish();
    }
    private void saveNewUser() {
        parseUser = ParseUser.getCurrentUser();
        parseUser.put("name", name);
        parseUser.setEmail(email);


        //Saving profile photo as a ParseFile
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        if (profilePic != null) {
            profilePic.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] data = stream.toByteArray();
            String thumbName = parseUser.getUsername().replaceAll("\\s+", "");
            final ParseFile parseFile = new ParseFile(thumbName + "_thumb.jpg", data);

            parseFile.saveInBackground((SaveCallback) e -> {
                parseUser.put("profileThumb", parseFile);
                parseUser.put("fbID",  id);
                //Finally save all the user details
                parseUser.saveInBackground(e1 -> Toast.makeText(getBaseContext(), "New user:" + name + " Signed up", Toast.LENGTH_SHORT).show());

            });
        }
        loadMapActivity();
    }

    private void getUserDetailsFromFB() {

        // Suggested by https://disqus.com/by/dominiquecanlas/
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,name,picture");


        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                parameters,
                HttpMethod.GET,
                response -> {
                    /* handle the result */
                    try {
                        Log.d("Response", response.getRawResponse());
                        id = response.getJSONObject().getString("id");
                        email = response.getJSONObject().getString("email");
                        name = response.getJSONObject().getString("name");


                        Bundle picParameters = new Bundle();
                        picParameters.putBoolean("redirect", false);
                        new GraphRequest(
                                AccessToken.getCurrentAccessToken(),
                                "/"+ id +"/picture?type=large",
                                picParameters,
                                HttpMethod.GET,
                                response1 -> {
                                    Log.d("Response", response1.getRawResponse());
                                    String pictureUrl = null;
                                    try {
                                        JSONObject data = response1.getJSONObject().getJSONObject("data");
                                        pictureUrl = data.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("Profile pic", "url: " + pictureUrl);

                                    photoAsync = new ProfilePhotoAsync(pictureUrl);
                                    photoAsync.delegate = SplashActivity.this;
                                    photoAsync.execute();
                                }
                        ).executeAsync();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
        ).executeAsync();

    }


    private void getUserDetailsFromParse() {
        parseUser = ParseUser.getCurrentUser();
        //Fetch profile photo
        try {
            //ParseFile parseFile = parseUser.getParseFile("profileThumb");
            //byte[] data = parseFile.getData();
            //Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            loadMapActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(MainActivity.this, "Welcome back " + mUsername.getText().toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void processFinish(Bitmap output) {
        profilePic = output;
        saveNewUser();
    }

    private void fbKeyLog() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.pnametala.debarembar",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {

        }
    }
}
