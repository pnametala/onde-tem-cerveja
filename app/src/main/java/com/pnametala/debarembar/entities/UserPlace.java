package com.pnametala.debarembar.entities;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseRelation;

/**
 * Created by visagio on 01/08/16.
 */
@ParseClassName("UserPlace")
public class UserPlace extends ParseObject{
    public UserPlace() {
        //default constructor is required
    }

    public String getName() {
        return getString("name");
    }
    public void setName(String name) {
        put("name", name);
    }

    public String getPlaceId() {
        return getString("placeId");
    }
    public void setPlaceId (String placeId) {
        put("placeId", placeId);
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("location");
    }
    public void setLocation(ParseGeoPoint point) {
        put("location", point);
    }
    public void setAddress(String address) { put("address", address); }
    public String getAddress() {return getString("address");}
    public void setPhone(String phone) {put("phone", phone);}
    public String getPhone() {return getString("phone");}
    public void setPlaceItem(PlaceItem item) {
        ParseRelation<PlaceItem> relation = getRelation("placeItem");
        relation.add(item);
    }
    public ParseRelation<PlaceItem> getPlaceItems() {
        return getRelation("placeItem");
    }

}
