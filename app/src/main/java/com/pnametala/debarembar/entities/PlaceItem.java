package com.pnametala.debarembar.entities;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by visagio on 31/07/16.
 */
@ParseClassName("PlaceItem")
public class PlaceItem extends ParseObject{

    public PlaceItem() {}

    public String getItemName() {
        return getString("name");
    }
    public void setItemName(String itemName) {
        put("name", itemName);
    }
    public double getItemPrice() {
        return getDouble("price");
    }
    public void setItemPrice(double itemPrice) {
        put("price", itemPrice);
    }
    public void setItemType(String itemType) {put("type", itemType);}
    public String getItemType() {return getString("type");}
    public void setItemComplement(String complement) {put("complement", complement);}
    public String getItemComplement() {return getString("complement");}
    public void createdBy(ParseUser user) { put("createdBy", user);}
    public ParseUser createdBy() {return (ParseUser) getParseObject("createdBy");}
    public void setRatingCount(int ratingCount)  { put("ratingCount", ratingCount);}
    public void getRatingCount() {getInt("ratingCount");}
}
