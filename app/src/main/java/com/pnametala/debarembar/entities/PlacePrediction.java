package com.pnametala.debarembar.entities;

/**
 * Created by visagio on 26/07/16.
 */
public class PlacePrediction {
    private String description;

    private String placeId;

    public PlacePrediction(){

    }
    public PlacePrediction(String placeId, String description)  {
        this.placeId = placeId;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }
}
