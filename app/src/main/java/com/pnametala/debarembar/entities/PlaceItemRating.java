package com.pnametala.debarembar.entities;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by visagio on 31/07/16.
 */
@ParseClassName("PlaceItemRating")
public class PlaceItemRating extends ParseObject{

    public PlaceItemRating() {}

    public void setPlaceItem(PlaceItem placeItem) {put("placeItem", placeItem);}
    public PlaceItem getPlaceItem(){ return (PlaceItem) getParseObject("placeItem");}
    public void setIsValid(boolean isValid) {put("isValid", isValid);}
    public boolean getIsValid() {return getBoolean("isValid");}
    public void createdBy(ParseUser user) { put("createdBy", user);}
    public ParseUser createdBy() {return (ParseUser) getParseObject("createdBy");}

    public void updateItem() {
        PlaceItem pi = getPlaceItem();
        int increment = (getIsValid()) ? 1 : -1;
        pi.increment("ratingCount", increment);
        pi.saveInBackground();
    }
}
