package com.pnametala.debarembar.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pnametala.debarembar.BaseActivity;
import com.pnametala.debarembar.R;
import com.pnametala.debarembar.entities.PlaceItem;
import com.pnametala.debarembar.entities.PlaceItemRating;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visagio on 31/07/16.
 */
public class ItemsInPlaceAdapter  extends ArrayAdapter<PlaceItem>  {
    private Context mContext;
    private int mResource;
    private ArrayList<PlaceItem> mItems;
    public ItemsInPlaceAdapter(Context context, ArrayList<PlaceItem> items) {
        super(context, R.layout.items_in_place, items);
        //resultList = new ArrayList<PlacePrediction>();
        mContext = context;
        mResource = R.layout.items_in_place;
        mItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlaceItem pi = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_in_place, parent, false);
        }

        // Lookup view for data population
        TextView itemName = (TextView) convertView.findViewById(R.id.item_name);
        TextView itemComplement = (TextView) convertView.findViewById(R.id.item_complement);
        TextView itemPrice = (TextView) convertView.findViewById(R.id.item_price);
        ImageView createdByPic = (ImageView) convertView.findViewById(R.id.item_create_by_picture);
        TextView createdBy = (TextView) convertView.findViewById(R.id.item_created_by);
        Button itemRating = (Button) convertView.findViewById(R.id.item_rating);

        // Populate the data into the template view using the data object
        itemName.setText(pi.getItemName());
        itemComplement.setText(pi.getItemComplement());
        itemPrice.setText("R$ " + String.format("%.2f", pi.getItemPrice()));
        ParseUser owner = pi.createdBy();
        try {
            owner.fetchIfNeeded();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        createdBy.setText(owner.get("name").toString());

        ParseFile parseFile = ParseUser.getCurrentUser().getParseFile("profileThumb");
        if(parseFile != null) {
            try {
                byte[] data = new byte[0];
                data = parseFile.getData();
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                createdByPic.setImageBitmap(Bitmap.createScaledBitmap(BaseActivity.generateBorders(bitmap), 60, 60, false));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        ParseQuery<PlaceItemRating> query = ParseQuery.getQuery(PlaceItemRating.class);
        query.whereEqualTo("createdBy", ParseUser.getCurrentUser());
        query.whereEqualTo("placeItem", pi);
        query.countInBackground((count, e) -> {
            if(e == null) {
                if(count > 0)
                    itemRating.setText("Avaliado");
            }
        });

        itemRating.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Avaliar " + pi.getItemName() + " " + pi.getItemComplement());
            builder.setMessage("Este preço está correto?");
            builder.setPositiveButton("Sim", (dialog, id) -> {
                ParseQuery<PlaceItemRating> userRating = ParseQuery.getQuery(PlaceItemRating.class);
                userRating.whereEqualTo("createdBy", ParseUser.getCurrentUser());
                userRating.whereEqualTo("placeItem", pi);
                userRating.getFirstInBackground((object, e1) -> {
                    if(e1 == null)  {
                        if(!object.getIsValid()) {
                            object.setIsValid(true);
                            saveIteRating(object, itemRating);
                        }
                    } else {
                        PlaceItemRating rating = new PlaceItemRating();
                        rating.setPlaceItem(pi);
                        rating.setIsValid(true);
                        rating.createdBy(ParseUser.getCurrentUser());
                        saveIteRating(rating, itemRating);
                    }
                });
                dialog.cancel();
            });

            builder.setNegativeButton("Não", (dialog, id) -> {
                ParseQuery<PlaceItemRating> userRating = ParseQuery.getQuery(PlaceItemRating.class);
                userRating.whereEqualTo("createdBy", ParseUser.getCurrentUser());
                userRating.whereEqualTo("placeItem", pi);
                userRating.getFirstInBackground((object, e1) -> {
                    if(e1 == null)  {
                        if(object.getIsValid()) {
                            object.setIsValid(false);
                            saveIteRating(object, itemRating);
                        }
                    } else {
                        PlaceItemRating rating = new PlaceItemRating();
                        rating.setPlaceItem(pi);
                        rating.setIsValid(false);
                        rating.createdBy(ParseUser.getCurrentUser());
                        saveIteRating(rating, itemRating);
                    }
                });
                dialog.cancel();
            });

            builder.setNeutralButton("Cancelar", (dialog, id) -> {
                dialog.cancel();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        return convertView;
    }

    public void refreshListView(List<PlaceItem> newItems){
        clear();
        addAll(newItems);
        notifyDataSetChanged();
    }



    private void saveIteRating(PlaceItemRating pir, Button rateButton) {
        pir.saveInBackground(e -> {
            if(e == null) {
                pir.updateItem();
                Toast.makeText(mContext, "Item avaliado com sucesso!", Toast.LENGTH_SHORT).show();
                rateButton.setText("Avaliado");
            } else {
                Toast.makeText(mContext, "Falha ao avaliar o item", Toast.LENGTH_SHORT).show();
                Log.e("PlaceItemRating", "Falha ao avaliar item: " + e.getMessage());
            }
        });
    }
}
