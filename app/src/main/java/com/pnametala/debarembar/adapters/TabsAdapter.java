package com.pnametala.debarembar.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.parse.ParseException;
import com.pnametala.debarembar.entities.PlaceItem;
import com.pnametala.debarembar.entities.UserPlace;
import com.pnametala.debarembar.fragments.AppetizersFragment;
import com.pnametala.debarembar.fragments.BeveragesFragment;

import java.util.List;
import java.util.Vector;

/**
 * Created by visagio on 16/08/16.
 */
//FragmentStatePagerAdapter
public class TabsAdapter extends FragmentStatePagerAdapter{
    private Context mContext;
    private Vector<View> mPages;
    public BeveragesFragment beveragesFragment;
    public AppetizersFragment appetizersFragment;
    public TabsAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        beveragesFragment = new BeveragesFragment();
        appetizersFragment = new AppetizersFragment();
    }
    /*public TabsAdapter(Context context, Vector<View> pages) {
        mContext = context;
        mPages = pages;
    }*/
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return  beveragesFragment;
            case 1:
                return appetizersFragment;
            default:
                return new Fragment();
        }
    }

    /*@Override
    public Object instantiateItem(ViewGroup container, int position) {
        View page = mPages.get(position);
        container.addView(page);
        return page;
    }*/
    @Override
    public int getCount() {
        //return mPages.size();
        return 2;
    }

    public void refresh(UserPlace place) {
        List<PlaceItem> beverages = null;
        List<PlaceItem> appetizers = null;
        try {
            beverages = place.getPlaceItems()
                    .getQuery()
                    .whereEqualTo("type", "Bebida")
                    .whereGreaterThan("ratingCount", -5)
                    .orderByAscending("name").find();
            appetizers =  place.getPlaceItems()
                    .getQuery()
                    .whereEqualTo("type", "Comida")
                    .whereGreaterThan("ratingCount", -5)
                    .orderByAscending("name").find();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        beveragesFragment.refresh(beverages);
        appetizersFragment.refresh(appetizers);
        notifyDataSetChanged();
    }
}
