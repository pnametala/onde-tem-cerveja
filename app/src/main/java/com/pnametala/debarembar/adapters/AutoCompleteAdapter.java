package com.pnametala.debarembar.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pnametala.debarembar.R;
import com.pnametala.debarembar.UserLocation;
import com.pnametala.debarembar.entities.PlacePrediction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by visagio on 24/07/16.
 */
public class AutoCompleteAdapter extends ArrayAdapter<PlacePrediction> implements Filterable {

    private static final String LOG_TAG = "Google API";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyA5lG-QfVzKDn4SBX6qrXDLkCvQEDqctXo";


    private ArrayList<PlacePrediction> resultList;
    private Context mContext;
    private int mResource;
    private GoogleApiClient mGoogleApiClient;
    public AutoCompleteAdapter(Context context) {
        super(context, R.layout.place_prediction);
        resultList = new ArrayList<PlacePrediction>();
        mContext = context;
        mResource = R.layout.place_prediction;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlacePrediction pp = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.place_prediction, parent, false);
        }

        // Lookup view for data population
        TextView placeId = (TextView) convertView.findViewById(R.id.place_id);
        TextView placeDescription = (TextView) convertView.findViewById(R.id.place_description);
        // Populate the data into the template view using the data object
        placeId.setText(pp.getPlaceId());
        placeDescription.setText(pp.getDescription());

        return convertView;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public PlacePrediction getItem(int position) {
        return resultList.get(position);
    }
    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.mGoogleApiClient = googleApiClient;
    }
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if( mGoogleApiClient == null || !mGoogleApiClient.isConnected() ) {
                    Toast.makeText( mContext, "Google API não está contectado", Toast.LENGTH_SHORT ).show();
                    return null;
                }

                if (constraint == null || constraint.length() == 0) return null;

                if (getCount() > 0) clear();

                //displayResults(constraint.toString());
                resultList = autocomplete(constraint.toString());
                filterResults.values = resultList;
                filterResults.count = resultList.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0 && constraint != null) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    private void displayResults(String query) {
        //Southwest corner to Northeast corner.
        //LatLngBounds bounds = new LatLngBounds( new LatLng( 39.906374, -105.122337 ), new LatLng( 39.949552, -105.068779 ) );

        //Filter: https://developers.google.com/places/supported_types#table3

        AutocompleteFilter.Builder filters = new AutocompleteFilter.Builder();
        filters.setTypeFilter(Place.TYPE_STREET_ADDRESS);

        PendingResult<AutocompletePredictionBuffer> result;
        LatLng latLng1 = new LatLng(UserLocation.LAT - 10, UserLocation.LONG - 10);
        LatLng latLng2 = new LatLng(UserLocation.LAT + 10, UserLocation.LONG + 10);

        result = Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query, new LatLngBounds(latLng1, latLng2), filters.build());
        result.setResultCallback (
                new ResultCallback<AutocompletePredictionBuffer>() {
                    @Override
                    public void onResult( AutocompletePredictionBuffer buffer ) {

                        if( buffer == null )
                            return;
                        if( buffer.getStatus().isSuccess() ) {
                            for( AutocompletePrediction prediction : buffer ) {
                                resultList.add(new PlacePrediction(prediction.getPlaceId(),prediction.getFullText(null).toString()));

                            }
                        }
                        //Prevent memory leak by releasing buffer
                        buffer.release();
                    }
                }, 60, TimeUnit.SECONDS );
    }

    private ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&location=" + UserLocation.LOCATION.getLatitude() + "," + UserLocation.LOCATION.getLongitude());
            sb.append("&rankby=distance");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                //System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                //System.out.println("============================================================");
                resultList.add(new PlacePrediction(predsJsonArray.getJSONObject(i).getString("place_id"), predsJsonArray.getJSONObject(i).getString("description")));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
}
