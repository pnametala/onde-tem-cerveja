package com.pnametala.debarembar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pnametala.debarembar.R;
import com.pnametala.debarembar.entities.PlaceItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visagio on 31/07/16.
 */
public class ItemsInPlaceEditorAdapter extends ArrayAdapter<PlaceItem>  {
    private Context mContext;
    private int mResource;
    private ArrayList<PlaceItem> mItems;
    public ItemsInPlaceEditorAdapter(Context context, ArrayList<PlaceItem> items) {
        super(context, R.layout.items_in_place, items);
        //resultList = new ArrayList<PlacePrediction>();
        mContext = context;
        mResource = R.layout.items_in_place_editor;
        mItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlaceItem pi = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_in_place_editor, parent, false);
        }

        // Lookup view for data population
        TextView itemId = (TextView) convertView.findViewById(R.id.item_id);
        TextView itemType = (TextView) convertView.findViewById(R.id.item_type);
        TextView itemName = (TextView) convertView.findViewById(R.id.item_name);
        TextView itemPrice = (TextView) convertView.findViewById(R.id.item_price);
        // Populate the data into the template view using the data object
        itemId.setText(pi.getObjectId());
        itemType.setText(pi.getItemType());
        itemName.setText(pi.getItemName());
        itemPrice.setText(String.format("%.2f", pi.getItemPrice()));

        return convertView;
    }

    public void refreshListView(List<PlaceItem> newItems){
        clear();
        addAll(newItems);
        notifyDataSetChanged();
    }
}
